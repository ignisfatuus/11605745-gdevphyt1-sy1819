﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vector  {

    // Use this for initialization
    public float x;
    public float y;
    [SerializeField] private float angle;
    [SerializeField] private float distance;
    private float Force;
    public Vector(float x, float y)
    {
        this.x = x;
        this.y = y;
    }
    public Vector()
    {

    }   

    public  Vector ConvertAngleToVector(float angle,float distance)
    {
        Vector newVector = new Vector();
        newVector.x = Mathf.Cos(angle * Mathf.Deg2Rad)*distance;
        newVector.y = Mathf.Sin(angle * Mathf.Deg2Rad)*distance;

        return newVector;
    }

    public  float VectorToAngle(float x,float y)
    {

        return (float)Mathf.Atan2(x, y);

    }

    public float GetMagnitude()
    {
        return (float)Mathf.Sqrt((x * x) + (y * y));
    }

    public float GetAngle()
    {
        return (float)Mathf.Atan2(x, y);
    }

    public float VectorToForce()
    {
        return Force;
    }
}
