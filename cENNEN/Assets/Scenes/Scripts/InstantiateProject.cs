﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateProject : MonoBehaviour {

    // Use this for initialization
    public GameObject Projectile;
    public GameObject Target;
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Shoot();
	}

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        Instantiate(Projectile, new Vector3(Target.transform.position.x, Target.transform.position.y, 0), gameObject.transform.rotation);
    }
}
