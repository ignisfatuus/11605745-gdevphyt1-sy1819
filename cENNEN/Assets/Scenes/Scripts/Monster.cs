﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour {
     public Vector2 Speed;
    public int Health;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 Direction;
        Direction.x = -1;
         Direction.y = Input.GetAxis("Vertical");
       
        Vector2 Velocity = new Vector2(Speed.x * Direction.x, Speed.y * Direction.y);
        //Rotate();
        transform.position += new Vector3(Velocity.x, Velocity.y);
         DestroyGO();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            Debug.Log("wtf");
            Health -= 20;
        }

    }

    void DestroyGO()
    {
        if(Health<=0)
        {
            Destroy(gameObject);
        }
    }
}
