﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mOVEMENT : MonoBehaviour {
    // Use this for initialization
    public Vector2 Acceleration = new Vector2();
    //  [SerializeField]private float angle;
    private Vector2 Velocity = new Vector2();
    private Vector2 Direction = new Vector2();
    private Vector2 RotationDirection = new Vector2();
    public Vector2 Force = new Vector2();
    private void Start()
    {

     
        StartCoroutine(Accelerate(1));

    }
    // Update is called once per frame
    void Update()
    {
        Apply2DForce();
        Movement();
        if (Velocity.x <= 0) Velocity.x = 0;

    }



    void Movement()
    {
        transform.position += new Vector3(Velocity.x, Velocity.y);
    }

    IEnumerator Accelerate(int WaitInSeconds)
    {
        while (true)
        {
            Debug.Log("hello");

            Velocity += Acceleration;
            yield return new WaitForSeconds(WaitInSeconds);
        }
    }
    void Apply2DForce()
    {
        Direction.x = Input.GetAxis("Horizontal");
        Direction.y = Input.GetAxis("Vertical");
  
        Velocity = new Vector2(Force.x * Direction.x, Force.y * Direction.y);
        Rotate();
        transform.position += new Vector3(Velocity.x, Velocity.y);
    }

    void Rotate()
    {
      
        
        if (Direction.x != 0 || Direction.y != 0) transform.up = new Vector3(0, Direction.y);
    }

}
