﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Tip : MonoBehaviour {
    public int Health;
    public Text GameOverText;
    public Button GameOverButton;
    public Move PlayerMove;
	// Use this for initialization
	void Start () {
        Health = 100;	
	}
	
	// Update is called once per frame
	void Update () {
		if (Health<=0)
        {
            GameOverText.gameObject.SetActive(true);
            GameOverButton.gameObject.SetActive(true);
            PlayerMove.Speed.y = 0;
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Monster")
        {
            Health -= 20;
          
        }
    }
}
