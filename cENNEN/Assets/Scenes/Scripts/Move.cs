﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    // Use this for initialization
    public Vector2 Speed;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 Direction;
        Direction.x = Input.GetAxis("Horizontal");
         Direction.y = Input.GetAxis("Vertical");
       
        Vector2 Velocity = new Vector2(Speed.x * Direction.x, Speed.y * Direction.y);
        //Rotate();
        transform.position += new Vector3(Velocity.x, Velocity.y);
    }
}
