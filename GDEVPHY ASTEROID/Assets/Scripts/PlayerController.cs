﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {


    public float tiltAngle;
    public float Smooth;
    public Vector3 Speed;
    private Movable PhysicsComponent;
	// Use this for initialization
	void Start ()
    {
      
        PhysicsComponent = GetComponent<Movable>();
        Speed *= Time.deltaTime;
      
    }
	
	// Update is called once per frameSSSSSS
	void Update ()
    {
        CharacterMovement();
    }

    void CharacterMovement()
    {
        Vector3 Direction;
        Direction.x = Input.GetAxis("Horizontal");
        Direction.y = Input.GetAxis("Vertical");
        Direction.z = 0;

        Direction.x *= Speed.x;
        Direction.y *= Speed.y;

        PhysicsComponent.Velocity = Direction;


        Debug.Log(PhysicsComponent.Velocity);
        this.gameObject.transform.position = PhysicsComponent.Position;
        Rotation();
    }

    void Rotation()
    {
        Vector3 MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.up = new Vector2(MousePosition.x, MousePosition.y);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Meteor")
        {
            Destroy(gameObject);
        }
    }
}
