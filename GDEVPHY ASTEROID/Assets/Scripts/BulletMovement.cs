﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour {

    Vector3 TargetPosition;
    public Vector3 MousePosition;
    public float Speed;
	// Use this for initialization
	void Start ()
    {
        Vector3 MousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        TargetPosition = MousePosition.normalized;
        TargetPosition *= 100;
    }

    // Update is called once per frame
    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, new Vector2(TargetPosition.x, TargetPosition.y), Speed * Time.deltaTime);
    }

    


}
