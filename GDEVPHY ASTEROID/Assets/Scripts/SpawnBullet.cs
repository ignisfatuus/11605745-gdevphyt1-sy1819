﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBullet : MonoBehaviour {

    public GameObject ObjectToSpawn;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        Shoot();
	}

    void Shoot()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(ObjectToSpawn, gameObject.transform.position,Quaternion.identity);
        }
    }
}
