﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMeteorDestroy : MonoBehaviour {


    public GameObject ChildMeteor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Instantiate(ChildMeteor, gameObject.GetComponent<Movable>().Position = new Vector3(gameObject.GetComponent<Movable>().Position.x - 0.2f, gameObject.GetComponent<Movable>().Position.y), Quaternion.identity);
            Instantiate(ChildMeteor, gameObject.GetComponent<Movable>().Position = new Vector3(gameObject.GetComponent<Movable>().Position.x + 0.2f, gameObject.GetComponent<Movable>().Position.y), Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
