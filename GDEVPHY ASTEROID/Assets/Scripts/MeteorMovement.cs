﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorMovement : MonoBehaviour {

    public float Speed;
    float smooth = 5.0f;
    float tiltAngle = 360f;
    private Movable PhysicsComponent;
	// Use this for initialization
	void Start () {
        Speed *= Time.deltaTime;
        PhysicsComponent = GetComponent<Movable>();
        StartCoroutine(AddForceOverTime(1));
	}
	
	// Update is called once per frame
	void Update ()
    {

        this.gameObject.transform.position = PhysicsComponent.Position;
        Rotate();
	}

    IEnumerator AddForceOverTime(int Seconds)
    {
        while (true)
        {
            PhysicsComponent.Velocity = (new Vector3(Random.Range(-Speed, Speed), Random.Range(-Speed, Speed), 0));
            yield return new WaitForSeconds(2);
        }
    }

    void Rotate()
    {
        // Smoothly tilts a transform towards a target rotation.
        float tiltAroundZ =  Time.frameCount + tiltAngle;
        

        Quaternion target = Quaternion.Euler(0, 0, tiltAroundZ);

        // Dampen towards the target rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);
    }
}
