﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    // Use this for initialization

    public float SpawnInterval = 1.0f;
    public int SpawnCount = 1;
    public GameObject ObjectToSpawn;


    private List<GameObject> spawned = new List<GameObject>();
    private List<GameObject> pooledObjects = new List<GameObject>();

    void Start()
    {
      
        StartCoroutine(SpawnTask());

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator SpawnTask()
    {
        while (true)
        {

            yield return new WaitForSeconds(SpawnInterval);
            for (int i = 0; i < SpawnCount; i++)
            {
                GameObject obj;
                if (pooledObjects.Count > 0)
                {
                    obj = pooledObjects[0];
                    pooledObjects.RemoveAt(0);
                    obj.SetActive(true);
                }
                else
                {
                    obj = Instantiate(ObjectToSpawn);
                }

               obj.GetComponent<Movable>().Position = new Vector3(Random.Range(-8,8),Random.Range(-4.5f,4.5f));
                spawned.Add(obj);
            }

        }
    }

    

    

}
