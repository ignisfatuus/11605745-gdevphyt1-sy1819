﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorMovement : MonoBehaviour {

    public float Speed;
    private Movable PhysicsComponent;
	// Use this for initialization
	void Start () {
        Speed *= Time.deltaTime;
        PhysicsComponent = GetComponent<Movable>();
        StartCoroutine(AddForceOverTime(1));
	}
	
	// Update is called once per frame
	void Update ()
    {

        this.gameObject.transform.position = PhysicsComponent.Position;
       
	}

    IEnumerator AddForceOverTime(int Seconds)
    {
        while (true)
        {
            PhysicsComponent.Velocity = (new Vector3(Random.Range(-Speed, Speed), Random.Range(-Speed, Speed), 0));
            yield return new WaitForSeconds(2);
        }
    }
}
