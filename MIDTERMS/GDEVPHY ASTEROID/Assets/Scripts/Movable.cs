﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movable: MonoBehaviour
{

    public Vector3 Position = new Vector3();
    public Vector3 Velocity = new Vector3();
    public Vector3 Acceleration = new Vector3();
    public float Mass = 0;
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdateMotion();
	}
    public void ApplyForce(Vector3 force)
    {
        // F = MA
        // A = F/M
        this.Acceleration += (force / Mass); //force accumulation
    }
    private void UpdateMotion()
    {
        this.Velocity += this.Acceleration;
        this.Position += this.Velocity;
        this.Acceleration *= 0;
    }
}
