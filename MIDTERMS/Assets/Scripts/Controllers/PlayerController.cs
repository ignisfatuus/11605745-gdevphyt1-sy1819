﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{


    public float Speed;
    public float RequiredDistance;
    public float RotationSpeed;
    public GameObject Target;
    private Vector3 PlayerMovement;
    private Animator PlayerAnimator;

    // Use this for initialization
    void Start()
    {
        PlayerAnimator = GetComponent<Animator>();
        Speed *= Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
      
        CheckDistance();
    }

    void Movement()
    {

        float HorizontalDirection = Input.GetAxis("Horizontal");
        float VerticalDirection = Input.GetAxis("Vertical");
  


        PlayerMovement.x = Speed * HorizontalDirection;
        PlayerMovement.y = Speed * VerticalDirection;
        PlayPlayerAnimation();
        transform.position += new Vector3(PlayerMovement.x, PlayerMovement.y);


    }

    void PlayPlayerAnimation()
    {
         Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
         float angle = Mathf.Atan2(direction.x, direction.y) * Mathf.Rad2Deg;

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            {
                if (angle >= 0)
                {
                    PlayerAnimator.Play("Right Walk");
                    return;
                }
                if (angle < 0)
                {
                    PlayerAnimator.Play("Left Walk");
                    return;
                }
            }
        }
        else
        {
            if (angle < 0)
            {
                PlayerAnimator.Play("Facing Left");
                return;
            }
            if (angle >= 0)
            {
                PlayerAnimator.Play("Facing Right");
                return;

            }
        }


    }
    void CheckDistance()
    {

    }
}
