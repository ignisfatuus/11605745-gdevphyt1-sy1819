﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour {

    public Camera cameraObject;

    public Transform TargetSelected;
    public Transform Player;
    public float RequiredDistance;
    public Inventory InvetoryManager;
    // Use this for initialization
    void Start()
    {
        cameraObject = GameObject.Find("Main Camera").GetComponent<Camera>();
        //   WeaponDurability = GameObject.Find("Player Weapon").GetComponent<Durability>();
    }

    // Update is called once per frame
    void Update()
    {
        OnClick();
    }

    public void OnClick()
    {

        if (Input.GetMouseButton(1))
        {
            RaycastHit hit;
            Ray ray = cameraObject.ScreenPointToRay(Input.mousePosition); // ray from camera to mouse position        
            if (Physics.Raycast(ray, out hit))
            {

                if (hit.collider.tag != "Arrow") return;

                Debug.Log("putang ina talaga");

                TargetSelected = hit.collider.gameObject.transform;

                // float Distance = Vector3.Distance(TargetSelected.position, Player.position);
                //  if (Distance > RequiredDistance) return;
                InvetoryManager.addToArrowList(hit.collider.gameObject);
                hit.collider.gameObject.SetActive(false);

            }

        }
    }
}
