﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBarUI : MonoBehaviour {


    public Image ImageBar;
    public Health PlayerHealth;
    float FillAmount;
    // Use this for initialization
    void Start()
    {
        ImageBar = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        FillBar();
    }

    void FillBar()
    {
        FillAmount = (float)PlayerHealth.CurrentHp / PlayerHealth.MaxHp;
        // Debug.Log(FillAmount);
        ImageBar.fillAmount = FillAmount;
    }
}
