﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreUI : MonoBehaviour {

    public Text UIText;
    public Score ScoreObject;
	// Use this for initialization
	void Start ()
    {
        UIText = GetComponent<Text>();
       
	}
	
	// Update is called once per frame
	void Update () {
        UIText.text = "Score:" + ScoreObject.ScoreAmount.ToString();
    }
}
